# Use Debian-based image for faster build
# https://pythonspeed.com/articles/alpine-docker-python/
FROM python:3-slim-buster

WORKDIR /usr/src/app

COPY app/requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY app/ .

RUN chown 900:900 . -R \
 && chmod a-w . -R

USER 900

CMD [ "python3", "-u", "./main.py" ]
