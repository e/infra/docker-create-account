#!/usr/bin/python3

import os
import sys
import json
import shlex
from threading import Thread
import logging
import requests
from requests.auth import HTTPBasicAuth
import paramiko
from pyramid.response import Response
from pyramid.request import Request

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
logging.getLogger('create-account').addHandler(ch)
paramiko.util.get_logger("paramiko").addHandler(ch)


def create_account(request: Request) -> Response:
    if request.params['auth'] != os.environ['CREATE_ACCOUNT_PASSWORD']:
        data = json.dumps({'success': False, 'message': 'no_auth'})
        return respond(401, data)

    print(f'Creating account for {request.params["target_email"]}, displayname='
          f'{request.params["displayname"]}, fallback email={request.params["fallback_email"]}')

    return create_account_perform(request.params['target_email'], request.params['password'],
                                  request.params['password_confirm'], request.params['displayname'],
                                  request.params['email_quota'], request.params['fallback_email'],
                                  request.params['nextcloud_quota'])


def create_account_perform(target_email: str, password: str, password_confirm: str,
                           displayname: str, email_quota: str, fallback_email: str,
                           nextcloud_quota: str) -> Response:
    target_username = target_email.split('@')[0]
    with open('forbidden_usernames') as f:
        if target_username in f.read().splitlines():
            data = json.dumps({'success': False, 'message': 'username_forbidden'})
            return respond(403, data)
    # create account via postfixadmin ssh
    with paramiko.SSHClient() as ssh:
        try:
            ssh.set_missing_host_key_policy(paramiko.MissingHostKeyPolicy)
            ssh.connect(hostname='postfixadmin', username='pfexec', password=os.environ['POSTFIXADMIN_SSH_PASSWORD'])

            stdin, stdout, stderr = ssh.exec_command(
                f'/postfixadmin/scripts/postfixadmin-cli mailbox view {shlex.quote(target_email)}')
            if b"error: the email is not valid!" not in stderr.read().lower():
                data = json.dumps({'success': False, 'message': 'username_taken'})
                return respond(403, data)

            stdin, stdout, stderr = ssh.exec_command(
                f'/postfixadmin/scripts/postfixadmin-cli mailbox add {shlex.quote(target_email)} ' +
                f'--password {shlex.quote(password)} --password2 {shlex.quote(password_confirm)} ' +
                f'--name {shlex.quote(displayname)} --quota {shlex.quote(email_quota)} --active 1 ' +
                f'--welcome-mail 0 --email-other {shlex.quote(fallback_email)}')
            print(stdout.read())
            print(stderr.read())
        except (ssh.SSHException, ssh.AuthenticationException, ssh.socket.error, ssh.BadHostKeyException) as e:
            print(e)
            data = json.dumps({'success': False, 'message': 'internal_error'})
            return respond(500, data)

    # Run Nextcloud API call in seperate thread. Needed because the API calls take 50+ seconds
    # each for unknown reasons. Instead we just run them in the background.
    Thread(target=call_nextcloud_api, args=(target_email, password, fallback_email, nextcloud_quota)).start()

    return respond(200, json.dumps({'success': True}))


def respond(status: int, message: str) -> Response:
    print(f'sending response status {status}, message {message}')
    return Response(status=status, body=message)


def call_nextcloud_api(target_email: str, password: str, fallback_email: str, nextcloud_quota: str):
    # Nextcloud only creates external accounts after the first login. We login through the API
    # to trigger the account creation.
    headers = {'OCS-APIRequest': 'true'}
    auth = HTTPBasicAuth(target_email, password)
    url = f'https://{os.environ["DOMAIN"]}/ocs/v1.php/cloud/users/{target_email}'
    requests.put(url, headers=headers, auth=auth)

    # Edit nextcloud account, set quota and email
    auth = HTTPBasicAuth(os.environ['NEXTCLOUD_ADMIN_USER'], os.environ['NEXTCLOUD_ADMIN_PASSWORD'])
    url = f'https://{os.environ["DOMAIN"]}/ocs/v1.php/cloud/users/{target_email.lower()}'
    r1 = requests.put(url, data={'key': 'email', 'value': fallback_email}, headers=headers, auth=auth)
    r2 = requests.put(url, data={'key': 'quota', 'value': nextcloud_quota}, headers=headers, auth=auth)
    print(r1.text)
    print(r2.text)
