#!/usr/bin/python3

import time
from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from create_account import create_account
from delete_account import delete_account, delete_account_form, confirm_delete_account

HOST_NAME = '0.0.0.0'
PORT_NUMBER = 9000


# https://gitlab.e.foundation/e/infra/ecloud-selfhosting/issues/64#note_26118
# https://docs.pylonsproject.org/projects/pyramid/en/latest/quick_tour.html
if __name__ == '__main__':
    with Configurator() as config:
        config.add_route('create-account', '/create-account', request_method='PUT')
        config.add_view(create_account, route_name='create-account')
        config.add_route('delete-account', '/delete-account')
        config.add_view(delete_account, route_name='delete-account')
        config.add_route('delete-account-form', '/delete-account-form', request_method='POST')
        config.add_view(delete_account_form, route_name='delete-account-form')
        config.add_route('confirm-delete-account', '/confirm-delete-account')
        config.add_view(confirm_delete_account, route_name='confirm-delete-account')
        app = config.make_wsgi_app()
    server = make_server(HOST_NAME, PORT_NUMBER, app)
    print(time.asctime(), 'Server started - %s:%s' % (HOST_NAME, PORT_NUMBER))
    server.serve_forever()
