#!/usr/bin/python3

import os
import time
import shlex
import random
import string
import sqlite3
from sqlite3 import Error, Connection
import smtplib
from xml.dom import minidom
import requests
from requests.auth import HTTPBasicAuth
import paramiko
from pyramid.response import Response, FileResponse
from pyramid.request import Request


SQLITE_DATABASE = r"/data/database.sqlite"
CONFIRMATION_LINK_VALIDITY_DURATION_SECONDS = 24 * 60 * 60


def create_connection() -> Connection:
    try:
        conn = sqlite3.connect(SQLITE_DATABASE)
        conn.execute("""CREATE TABLE IF NOT EXISTS confirm_delete_codes (
                        id INTEGER PRIMARY KEY,
                        user_email TEXT NOT NULL,
                        confirmation_code TEXT NOT NULL,
                        creation_timestamp INTEGER NOT NULL
                        ); """)
        return conn
    except Error as e:
        print(e)


def delete_account(request: Request) -> Response:
    return FileResponse(path='delete-account.html', content_type='text/html')


def delete_account_form(request: Request) -> Response:
    email = request.params['email']
    password = request.params['password']
    url = f'https://{os.environ["DOMAIN"]}/ocs/v1.php/cloud/users/{email}'
    r = requests.get(url, headers={'OCS-APIRequest': 'true'}, auth=HTTPBasicAuth(email, password))
    xmldoc = minidom.parseString(r.text)
    if xmldoc.getElementsByTagName('statuscode')[0].lastChild.nodeValue == '100':
        confirmation_code = ''.join(random.choice(string.ascii_lowercase) for i in range(20))
        conn = create_connection()
        conn.execute(
            "INSERT INTO confirm_delete_codes(user_email, confirmation_code, creation_timestamp) VALUES (?, ?, ?)",
            (email, confirmation_code, int(time.time())))
        conn.commit()
        conn.close()
        confirmation_link = f'https://{os.environ["DOMAIN"]}/confirm-delete-account?email={email}&confirmation_code={confirmation_code}'
        print('confirmation_link=' + confirmation_link)
        fallback_email = xmldoc.getElementsByTagName('email')[0].lastChild.nodeValue
        return send_confirmation_email(fallback_email, confirmation_link)
    else:
        return Response(status=403, body="Invalid email or password")


def send_confirmation_email(email: str, confirmation_link: str):
    sender = os.environ['SMTP_FROM']
    message = f"""From: {sender}
    To: {email}
    Subject: /e/ account deletion

    Please click the following link to delete your account. If you do not want to delete your account, 
    ignore this email.

    {confirmation_link}
    """
    try:
        smtp = smtplib.SMTP(os.environ['SMTP_HOST'])
        smtp.connect(os.environ['SMTP_HOST'])
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login(user=sender, password=os.environ['SMTP_PASSWORD'])
        smtp.sendmail(sender, [email], message)
        smtp.quit()
        return Response(status=200, body=f'Sent confirmation email to {email}')
    except smtplib.SMTPException as e:
        print(e)
        return Response(status=500, body=f'Failed to send confirmation email')


def confirm_delete_account(request: Request) -> Response:
    email = request.params['email']
    confirmation_code = request.params['confirmation_code']
    conn = create_connection()
    c = conn.execute(
        "SELECT EXISTS(SELECT 1 FROM confirm_delete_codes WHERE user_email=? AND confirmation_code=? AND creation_timestamp>?)",
        (email, confirmation_code, int(time.time()) - CONFIRMATION_LINK_VALIDITY_DURATION_SECONDS))
    exists = c.fetchone()[0]
    conn.close()
    if exists == 1:
        auth = HTTPBasicAuth(os.environ['NEXTCLOUD_ADMIN_USER'], os.environ['NEXTCLOUD_ADMIN_PASSWORD'])
        url = f'https://{os.environ["DOMAIN"]}/ocs/v1.php/cloud/users/{email.lower()}'
        r1 = requests.delete(url, headers={'OCS-APIRequest': 'true'}, auth=auth)
        print(r1.text)
        xmldoc = minidom.parseString(r1.text)
        if xmldoc.getElementsByTagName('statuscode')[0].lastChild.nodeValue != '100':
            return Response(status=500, body="Internal server error")
        with paramiko.SSHClient() as ssh:
            try:
                ssh.set_missing_host_key_policy(paramiko.MissingHostKeyPolicy)
                ssh.connect(hostname='postfixadmin', username='pfexec', password=os.environ['POSTFIXADMIN_SSH_PASSWORD'])

                stdin, stdout, stderr = ssh.exec_command(
                    f'/postfixadmin/scripts/postfixadmin-cli mailbox delete {shlex.quote(email)}')
                print(stdout.read())
                print(stderr.read())
            except (ssh.SSHException, ssh.AuthenticationException, ssh.socket.error, ssh.BadHostKeyException) as e:
                print(e)
                return Response(status=500, body="Internal server error")
        return Response(status=200, body="Account successfully deleted")
    else:
        return Response(status=403, body="Invalid email or confirmation code")
