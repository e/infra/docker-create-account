**January 2020 update:** This project is deprecated. It is only kept here for compatibility for ecloud-selfhost installations that may still need it.

Utility for creating a new user account in postfixadmin, and setting account
values via Nextcloud.

## Running
```
  create-account:
    image: docker-create-account:0.1.0
    container_name: create-account
    environment:
      - NEXTCLOUD_ADMIN_USER=${NEXTCLOUD_ADMIN_USER}
      - NEXTCLOUD_ADMIN_PASSWORD=${NEXTCLOUD_ADMIN_PASSWORD}
      - POSTFIXADMIN_SSH_PASSWORD=${POSTFIXADMIN_SSH_PASSWORD}
      - DOMAIN=${DOMAIN}
      - CREATE_ACCOUNT_PASSWORD=${CREATE_ACCOUNT_PASSWORD}
      - SMTP_HOST=${SMTP_HOST}
      - SMTP_FROM=${SMTP_FROM}
      - SMTP_PASSWORD=${SMTP_PASSWORD}
    volumes:
      - /mnt/repo-base/volumes/create-account:/data
    networks:
      - serverbase
    depends_on:
      - nextcloud
      - postfixadmin
```
## Usage

Send a `PUT` request to `example.com:9000/create-account`. Required parameters:
```
auth             # must be the same as the CREATE_ACCOUNT_PASSWORD env var (for client authorization)
target_email     # email to create (including domain)
password         # user password
password_confirm # user password confirmation
displayname      # user displayname
email_quota      # email storage quota in postfix
fallback_email   # email for password reset in Nextcloud
nextcloud_quota  # storage quota in Nextcloud
```

You can use the following command to make a request:
```
curl -X PUT https://example.com:9000/create-account \
    -d auth=$CREATE_ACCOUNT_PASSWORD -d target_email=user@example.com -d \
    password=pw -d password_confirm=pw -d displayname=name -d email_quota=quota \
    -d fallback_email=user@otherdomain.com -d nextcloud_quota=-1
```

## Response values:

On success, status code will be 200 and response body:
```
'{"success": true}'
```

On failure, status code will be 4xx or 5xx and response body one of the following:
```
{"success": false, "message": "username_forbidden"} # this username is not allowed
{"success": false, "message": "username_taken"} # this username is already taken
{"success": false, "message": "internal_error"} # failed to connect to postfixadmin or Nextcloud
{"success": false, "message": "no_auth"} # auth missing or wrong
```

# License

The project is licensed under [AGPL](LICENSE).
